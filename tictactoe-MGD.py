from turtle import *

speed(0)

#Board Functions

def square(size, sq_color):
    # Copy your square function from board_2.py here
    begin_fill()
    fillcolor(sq_color)

    for i in range(1,5):
        forward(size)
        right(90)
    end_fill()

def next_color(current_color):
    # Copy your next_color function from color.py here
    if current_color == "pink":
        return "lime"
    else:
        return "pink"

def row(number, sq_size, sq_color):
    # Copy your row function from board_2.py here.
    # You will need to modify it so it changes the color after each square is drawn
    fillcolor(sq_color)
    for i in range(number):
       square(sq_size, sq_color)
       forward(sq_size)
       sq_color = next_color(sq_color)
       

#Piece Functions

def drawstar ():
    forward(400/3/2)
    begin_fill()
    fillcolor("dodger blue")
    for i in range(5):
        forward(30)
        left(72)
        forward(30)
        right(144)
    end_fill()

def drawheart ():
    fillcolor("dark violet")
    begin_fill()
    setheading(90)
    right(30)
    forward(72)
    left(30)
    circle(18, 180)
    left(180)
    circle(18, 180)
    left(30)
    forward(72)
    end_fill()




def player():
    if player == 'player1':
        drawstar()

    elif player == 'player2':
        drawheart()


#Draw Board
penup()
goto(-200, 200)
pendown()
square(400, "pink")
first_color = "lime"
for i in range(3):
    row(3, 400/3, first_color)
    first_color = next_color(first_color)

    penup()
    back(400)
    right(90)
    forward(400/3)
    left(90)
    pendown()

penup()
goto(-200, 200)
pendown()

#Define Go To
def goto_square(x,y):
    x = int(x)
    y = int(y)
    board_size = 400
    squares_across = 3

    board_x = -200 + (x-1) * (board_size / squares_across)
    board_y = 200 - (y-1) * (board_size / squares_across)
    penup()
    goto(board_x, board_y)

    pendown()
 
#Draw Pieces
for i in range(9):
    rowask1= int(input("What Row do you choose? "))
    rowask2= int(input("what Column do you choose? "))
    penup()
    goto(rowask1, rowask2)
    pendown()
    

done()